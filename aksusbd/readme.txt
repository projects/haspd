HASP/Hardlock Daemon for Linux/x86 (July 2004)
==============================================

Installation
------------

1) If parallel key access is required, install the aksparlnx
   kernel mode driver.
2) Mount the usbdevfs:
   mount -t usbdevfs none /proc/bus/usb
3) Execute aksusbd as root user.
   The daemon forks, and runs in the background.

******************
HASP Daemon V1.8.1
******************

New Features
============
Supports SuSE 9.1


****
V1.8
****

New Features
============
Support for HASP HL.


Versions
--------

aksusbd	version: 1.8.1


Known issues
------------

On SuSE 9.0 systems, unplugging and and reconnecting USB devices can
cause the hotplug daemon to hang.