
/*****************************************************************************
 *
 * demo program for HASP4 time services
 *   (most of the servoces also work with older TimeHASP models)
 *
 * Copyright (c) Aladdin Knowledge Systems Ltd.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#ifdef DOS
#include <dos.h>
#endif
#include "hasp.h"           /* definitions for the HASP interface        */


int SeedCode        = 100;  /* the seed code - you can enter any integer */
int PortNum         =   0;  /* search all ports                          */
int Pass1           =   0;  /* the first HASP password                   */
int Pass2           =   0;  /* the second HASP password                  */
int p1, p2, p3, p4;
int IsTimeHASP      =   0;

int year, month, day, hour, min, sec;
int saved_year, saved_month, saved_day, saved_hour, saved_minute, saved_second;

#define MEMO_BUFFER_SIZE               48 /* size in bytes */
unsigned char MemoBuffer[MEMO_BUFFER_SIZE];
unsigned char EncryptionBuffer[4096 +1];

/* encrypted block of test data to be decrypted */
/* with the HaspEncodeData service              */
#define DATA_LEN 38
unsigned char EncryptedData[] =
  { 0x0a, 0xa8, 0x36, 0x3b, 0x68, 0xdf, 0x5b, 0x54,
    0x25, 0xb4, 0x11, 0xad, 0x8c, 0xfa, 0xa1, 0xb2,
    0x43, 0x6d, 0xcd, 0x5f, 0xb9, 0xdd, 0xab, 0x32,
    0xdb, 0xc2, 0xca, 0x49, 0x8d, 0x8e, 0xbb, 0x60,
    0x71, 0x0f, 0x88, 0x88, 0xf8, 0x14 };

/*****************************************************************************/

/* helper function: dumps a given block of data, in hex and ascii */

void dump (char* data, int datalen, char* margin);
void dump (char* data, int datalen, char* margin)
{
  int             i, j;
  unsigned char   b;
  char            s[20];

  if (datalen == 0) return;

  s[0] = 0;
  j = 0;
  for (i=0; i<datalen; i++) {
    if (j == 0) printf(margin);
    b = data[i];
    if ( (b < 32) || (b > 127)) s[j] = '.'; else s[j] = b;
    s[j+1] = 0;
    printf("%02X ", b);
    j++;
    if (((j & 3) == 0) && (j < 15))printf("| ");
    if (j > 15) { printf(" [%s]\n", s); j=0; s[0] = 0; }
  }
  if (j) {
    while (j < 16) {
      printf("   ");
      j++;
      if (((j & 3) == 0) && (j < 15))printf("| ");
    }
    printf(" [%s]\n", s);
  }
}

/*****************************************************************************/

/* helper function: reads time and date from HASP key */

int TimeHasp_GetDateTime (int* year, int* month, int* day,
                          int* hour, int* min,   int* sec)
{
  p1 = p2 = p3 = p4 = 0;
  hasp(TIMEHASP_GETTIME, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);
  if (p3) return 0;

  *hour = p4;
  *min  = p2;
  *sec  = p1;

  p1 = p2 = p3 = p4 = 0;
  hasp(TIMEHASP_GETDATE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) return 0;

  *day   = p1;
  *month = p2;
  if (p4 < 90) *year = p4 + 2000; else *year = p4 + 1900;

  return 1;
}
/*****************************************************************************/

int main(int argc, char *argv[])
{
  int i;

  printf("\nThis is a simple demo program for the HASP4 Time key\n");
  printf("Copyright (c) Aladdin Knowledge Systems Ltd.\n");

  /* check for optional parameters */

  switch(argc) {
    case 4: PortNum = atoi(argv[3]);
    case 3: Pass2 = atoi(argv[2]);
    case 2: Pass1 = atoi(argv[1]);
  break;

    default:
      printf("\nSyntax: %s <Pass1> <Pass2> <PortNum>\n",argv[0]);
  }


  if ((unsigned)Pass1 < 3 && Pass2 < 2) {
    if (Pass2 == 0) {
      switch(Pass1) {
        case 0: Pass1 = 15417; Pass2 =  9632; break;    /* 0 - DemoMA */
        case 1: Pass1 = 29875; Pass2 = 28774; break;    /* 1 - DemoMB */
        case 2: Pass1 = 29313; Pass2 = 23912; break;    /* 2 - DemoMC */
      }
    }
    else {
      switch(Pass1) {
        case 0: Pass1 = 5932;  Pass2 = 25657; break;    /* 0 - Demo3A */
        case 1: Pass1 = 20580; Pass2 = 22012; break;    /* 1 - Demo3B */
        case 2: Pass1 = 10038; Pass2 = 15697; break;    /* 2 - Demo3C */
      }
    }
  }

  printf("\nUsing Passwords    %d - %d\n", Pass1, Pass2);
  if (PortNum) printf("Using PortNum      %d\n", PortNum);
  printf("\n");

/******************************************************************************
 * ishasp
 *   Checks whether any HASP key is connected to the computer.
 */

  printf("LOCALHASP_ISHASP              : ");

  p1 = p2 = p3 = p4 = 0; /* no input parameters */
  hasp(LOCALHASP_ISHASP, 0, PortNum, 0, 0, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
    if (p3 == HASPERR_VERSION_MISMATCH) {
      printf("Old HASP driver found - please update driver to actual version.\n");
    }
    exit(1);
  } else  {
    printf("OK, result: %d\n", p1);
  }

 /*****************************************************************************
 * HASPStatus
 *   checks whether specified HASP key is attached,
 *   returns API version and HASP type
 */

  printf("LOCALHASP_HASPSTATUS          : ");
  p1 = p2 = p3 = p4 = 0; /* no input parameters */

  hasp(LOCALHASP_HASPSTATUS, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  printf("API version number is %d.%d\n", p4 / 1000, p4 % 1000);
  printf("                                port number %d\n", p3);

  if (p3 == 0) { /* if port number = 0, HASP not found */
    printf("\nThe requested HASP key was not found.\n");
    printf("Program aborted.\n");
    return(1);
  }

  printf("                                Key type: ");
  switch (p2) {

    case 0:
      printf("HASP4 Std."); break;

    case 1:
      printf("HASP4");
      switch(p1) {
        case 1: printf(" M1"); break;
        case 4: printf(" M4"); break;
      }
      break;

    case 5:
      printf("HASP4 Time");
      IsTimeHASP = 1;
      break;
  }

  printf("\n");

  if (!IsTimeHASP) {
    printf("\nThe connected Key is not a TimeHASP.\n");
    printf("Program aborted.\n");
    return(1);
  }

/*****************************************************************************
 *
 * HASPGENERATION
 *   checks whether the connected key is a HASP4 or an older model
 *
 */

  printf("LOCALHASP_HASPGENERATION      : ");

  p1 = p2 = p3 = p4 = 0;
  hasp(LOCALHASP_HASPGENERATION, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
    exit(1);
  } else  {
    switch (p1) {
      case 0: printf("OK, HASP3 is connected.\n"); break;
      case 1: printf("OK, HASP4 is connected.\n"); break;
    }
  }


/*****************************************************************************
 * TIMEHASP_HASPID
 *   reads the TimeHASP ID Number
 *   the ID Number is a 32 bit integer which is calculated
 *   as follows: ID = p1 + 65536 * p2
 */

  printf("TIMEHASP_HASPID               : ");

  hasp(TIMEHASP_HASPID, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed. Error number: %d. ", p3);
  } else {
    long ID;

    ID = p2;
    ID <<= 16;
    ID |= (unsigned) p1;
    printf("%ld (decimal), %#lx (hex)\n", ID, ID);
  }


/*****************************************************************************/
/*
    Memory services
    ---------------
    Caution: If you are using the READMEMO and WRITEMEMO services in a cross
    platform environment with different byte orders, results might not be
    as expected due to swapped bytes. Please use instead, the READBLOCK and
	  WRITEBLOCK services and consult the HASP Programmer's Guide.

*/

/*****************************************************************************
 * MEMOHASP_WRITEMEMO
 *   writes a word of data to the HASP memory
 */

  p1 = 1;             /* The address to which the data word will be written  */
  p2 = 0x1234;        /* The value to be written                             */

  printf("MEMOHASP_WRITEMEMO            : value 0x%04X to memory word %d : ", p2, p1);

  hasp(MEMOHASP_WRITEMEMO, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed. Error number: %d. ", p3);
  } else {
    printf("OK.\n");
  }

/*****************************************************************************
 * MEMOHASP_READMEMO
 *   reads a word of data from the HASP memory
 */

  p1 = 1;  /* the address of the data word to be read */
  p2 = 0;
  printf("MEMOHASP_READMEMO             : memory word %d :", p1);

  hasp(MEMOHASP_READMEMO, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3)  {
    printf("Failed. Error number: %d.\n ", p3);
  } else {
    printf( "%d (dec), %04X (hex).\n", p2, p2 );
  }

/*****************************************************************************
 * MEMOHASP_READBLOCK
 *   reads a memory block
 */

  printf("\nMEMOHASP_READBLOCK  : ");

  p1 = 0;                        /* start address          */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* buffer length in words */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  /* fill buffer with zeroes to ensure that buffer content */
  /* was modified by the READBLOCK service                 */
  memset(&MemoBuffer, 0, sizeof(MemoBuffer));

  hasp(MEMOHASP_READBLOCK, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(MemoBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * MEMOHASP_WRITEBLOCK
 *   writes a block of data to the HASP memory
 */

  printf("\nIncrementing every byte in memory buffer\n");
  for ( i = 0; i < MEMO_BUFFER_SIZE; i++ ) MemoBuffer[i]++;

  printf("MEMOHASP_WRITEBLOCK : ");
  p1 = 0;                        /* Start address                */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* Buffer length in words       */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  hasp(MEMOHASP_WRITEBLOCK, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed. Error number: %d.\n", p3);
  } else {
    printf("OK.\n");
  }

/*****************************************************************************
 * MEMOHASP_READBLOCK
 *   reads a block of data from the HASP memory
 */

  printf("\nMEMOHASP_READBLOCK  : ");

  p1 = 0;                        /* start address */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* buffer length in words */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  /* fill buffer with zeroes to ensure that buffer content */
  /* was modified by the READBLOCK service                 */
  memset(&MemoBuffer, 0, sizeof(MemoBuffer));

  hasp(MEMOHASP_READBLOCK, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(MemoBuffer, DATA_LEN, "    ");
  }


/*****************************************************************************
 *  HASP4 Time memory access
 *    these services access the 16-byte time chip memory
 *****************************************************************************
 *
 *  TIMEHASP_READBYTE
 *    reads one byte from the time chip memory
  */

  printf("\nTIMEHASP_READBYTE   : ");

  p1 = 0;  /* memory address, 0..15 */
  p2 = 0;  /* unused                */
  p3 = 0;  /* unused                */
  p4 = 0;  /* unused                */

  hasp(TIMEHASP_READBYTE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK. result= %d (0x%02X)\n", p2, p2);
  }

/*****************************************************************************
 * TIMEHASP_WRITEBYTE
 *   writes one byte to the time chip memory
 *
 */

  printf("\nTIMEHASP_WRITEBYTE  : ");

  p1 = 0;               /* memory address, 0..15   */
  p2 = (p2+1) & 0xFF;   /* data byte to be written */
  p3 = 0;               /* unused                  */
  p4 = 0;               /* unused                  */

  hasp(TIMEHASP_WRITEBYTE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
  }

/*****************************************************************************
 * TIMEHASP_READBLOCK
 *   reads a block of 16 bytes from the time chip memory
 */

  printf("\nTIMEHASP_READBLOCK  : ");
  p1 =  0;  /* start address                           */
  p2 = 16;  /* memory block length, in bytes (max. 16) */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;    /* unused  */
    p4 = (int)&MemoBuffer;
  #endif

  memset(&MemoBuffer, 0, sizeof(MemoBuffer));
  hasp(TIMEHASP_READBLOCK, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(MemoBuffer, 16, "    ");
  }

/*****************************************************************************
 * TIMEHASP_WRITEBLOCK
 *   writes a block of 16 bytes to the time chip memory
 */

  printf("\nTIMEHASP_WRITEBLOCK   : ");

  p1 =  0;  /* start address                           */
  p2 = 16;  /* memory block length, in bytes (max. 16) */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  hasp(TIMEHASP_WRITEBLOCK, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
  }


/*****************************************************************************
 * Encoding/decoding Data
 *   (these services are available for HASP4 keys only)
 *****************************************************************************
 *
 * LOCALHASP_DECODEDATA
 *   decodes a block of data using the HASP key
 *   block size must be at least 8 bytes
 */

  memcpy(&EncryptionBuffer, &EncryptedData, DATA_LEN);
  printf("\nOriginal encrypted data :\n");
  dump(EncryptionBuffer, DATA_LEN, "    ");

  printf("\nLOCALHASP_DECODEDATA : ");

  p1 = 0;         /* unused                         */
  p2 = DATA_LEN;  /* number of bytes to decode, >= 8 */

  #if defined(__16BIT__)
    p3 = FP_SEG(EncryptionBuffer);
    p4 = FP_OFF(EncryptionBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)EncryptionBuffer;
    p3 = ((unsigned long)EncryptionBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&EncryptionBuffer;
  #endif

  hasp(LOCALHASP_DECODEDATA, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(EncryptionBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * LOCALHASP_ENCODEDATA
 *   encodes a block of data using the HASP key
 *   block size must be at least 8 bytes
 */

  printf("\nLOCALHASP_ENCODEDATA : ");

  p1 = 0;        /* unused                          */
  p2 = DATA_LEN; /* number of bytes to encode, >= 8 */

  #if defined(__16BIT__)
    p3 = FP_SEG(EncryptionBuffer);
    p4 = FP_OFF(EncryptionBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)EncryptionBuffer;
    p3 = ((unsigned long)EncryptionBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&EncryptionBuffer;
  #endif

  hasp(LOCALHASP_ENCODEDATA, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(EncryptionBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * Time and Date services
 *   these services are available for HASP3 and HASP4 Time keys
 *
 * TIMEHASP_GETTIME
 *   read current time from HASP
 */

  printf("\nTIMEHASP_GETTIME            : ");

  p1 = p2 = p3 = p4 = 0; /* no input values */
  hasp(TIMEHASP_GETTIME, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK, hour: %02d  min  : %02d  sec : %02d\n", p4, p2, p1);
  }

/*****************************************************************************
 * TIMEHASP_GETDATE
 *   reads the current date from HASP
 *   the year is read as two digits
 */

  printf("TIMEHASP_GETDATE            : ");

  p1 = p2 = p3 = p4 = 0;  /* no input values */
  hasp(TIMEHASP_GETDATE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);


  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK, day : %02d  month: %02d  year: %02d\n", p1, p2, p4);
  }

/*****************************************************************************/

  /* reads time/date and saves them for later restore */

  TimeHasp_GetDateTime(&saved_year, &saved_month, &saved_day,
                     &saved_hour, &saved_minute, &saved_second);

/*****************************************************************************
 * TIMEHASP_SETTIME
 *   set the time of the HASP real time clock
 */

  printf("TIMEHASP_SETTIME            : ");

  p1 = 30;     /* seconds */
  p2 = 20;     /* minutes */
  p3 = 0;      /* unused  */
  p4 = 10;     /* hour    */
  hasp(TIMEHASP_SETTIME, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************
 * TIMEHASP_SETDATE
 *   sets the date of the HASP real time clock
 *   the year must be set as a two digit value
 */

  printf("TIMEHASP_SETDATE            : ");

  p1 = 1;            /* day                   */
  p2 = 2;            /* month                 */
  p3 = 0;            /* unused                */
  p4 = 2003 % 100;   /* year, two digits (03) */

  hasp(TIMEHASP_SETDATE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************/

  /* reads and displays the time we just set */

  TimeHasp_GetDateTime(&year, &month, &day, &hour, &min, &sec);
  printf("Reading time and date again : ");
  printf("%02d:%02d:%02d    %d %d %d\n", hour, min, sec, month, day, year);

/*****************************************************************************/

  /* sets time and date back to the original values */

  printf("TIMEHASP_SETTIME            : ");

  p1 = saved_second;     /* seconds */
  p2 = saved_minute;     /* minutes */
  p3 = 0;                /* unused  */
  p4 = saved_hour;       /* hour    */
  hasp(TIMEHASP_SETTIME, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK.\n");
  }


  printf("TIMEHASP_SETDATE            : ");

  /* sets date/time back to the values we've read before */
  p1 = saved_day;        /* day          */
  p2 = saved_month;      /* month        */
  p3 = 0;                /* unused       */
  p4 = saved_year % 100; /* 2 digit year */
  hasp(TIMEHASP_SETDATE, SeedCode, PortNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************
 * checks for running clock
 *   waits for change of second, then displays date/time
 *   (repeated 5 times)
 */

 printf("\n");

 {
   int oldsec = 99;
   int i;

   for (i=0; i<5; i++) {
     do {
       TimeHasp_GetDateTime(&year, &month, &day, &hour, &min, &sec);
     } while (sec == oldsec);
     oldsec = sec;
     printf("Check for running clock     : %02d:%02d:%02d    %d %d %d\n",
            hour, min, sec, month, day, year);
   } /* for i */
 }

/*****************************************************************************/

return 0;

} /* eof */
