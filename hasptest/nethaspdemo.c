
/*****************************************************************************
 *
 * demo program for HASP4 Net services
 *   (most of the services also work with older HASP3 Net models)
 *
 * Copyright (c) Aladdin Knowledge Systems Ltd.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#ifdef DOS
#include <dos.h>
#endif
#include "hasp.h"               /* definitions for the HASP interface        */

int     SeedCode        = 100;  /* the seed code - you can enter any integer */
int     ProgNum         =   1;  /* the program number for NetHASP            */
int     Pass1           =   0;  /* the first HASP password                   */
int     Pass2           =   0;  /* the second HASP password                  */
int     p1, p2, p3, p4;

short int *__ctype_toupper = NULL;

#define MEMO_BUFFER_SIZE 48     /* size in bytes */
unsigned char MemoBuffer[MEMO_BUFFER_SIZE];
unsigned char EncryptionBuffer[4096 +1];

unsigned char ServerName [256] = "";
unsigned char IniName [256] = "";

/* encoded block of test data to be decrypted */
/* with the HaspEncodeData function           */
#define DATA_LEN 38
unsigned char EncryptedData[] =
  { 0x0a, 0xa8, 0x36, 0x3b, 0x68, 0xdf, 0x5b, 0x54,
    0x25, 0xb4, 0x11, 0xad, 0x8c, 0xfa, 0xa1, 0xb2,
    0x43, 0x6d, 0xcd, 0x5f, 0xb9, 0xdd, 0xab, 0x32,
    0xdb, 0xc2, 0xca, 0x49, 0x8d, 0x8e, 0xbb, 0x60,
    0x71, 0x0f, 0x88, 0x88, 0xf8, 0x14 };

/*****************************************************************************/

/* helper function: dumps a given block of data, in hex and ascii */

void dump (char* data, int datalen, char* margin)
{
  int             i, j;
  unsigned char   b;
  char            s[20];

  if (datalen == 0) return;

  s[0] = 0;
  j = 0;
  for (i=0; i<datalen; i++) {
    if (j == 0) printf(margin);
    b = data[i];
    if ( (b < 32) || (b > 127)) s[j] = '.'; else s[j] = b;
    s[j+1] = 0;
    printf("%02X ", b);
    j++;
    if (((j & 3) == 0) && (j < 15))printf("| ");
    if (j > 15) { printf(" [%s]\n", s); j=0; s[0] = 0; }
  }
  if (j) {
    while (j < 16) {
      printf("   ");
      j++;
      if (((j & 3) == 0) && (j < 15))printf("| ");
    }
    printf(" [%s]\n", s);
  }
}

/*****************************************************************************/

int main(int argc, char *argv[])
{
  int i;

  printf("\nThis is a simple demo program for the HASP4 Net key\n");
  printf("Copyright (c) Aladdin Knowledge Systems Ltd.\n");

  switch(argc) {
    case 3: strcpy(ServerName, argv[2]);
    case 2: ProgNum = atoi(argv[1]);
  break;

    default:
      printf("\nSyntax: %s <ProgNum> <ServerName>\n",argv[0]);
  }


    if (Pass2 == 0) {
      Pass1 = 8587;
      Pass2 = 21674;
     }

  /* printf("\nUsing Passwords    %d - %d\n", Pass1, Pass2); */
  if (ProgNum) printf("Using ProgNum      %d\n", ProgNum);
  if (strlen(ServerName)) printf("Using Server Name  \"%s\"\n", ServerName);
  printf("\n");

/*****************************************************************************
 * HASPAPI_VERSION
 *   returns HASP API version number
 */

  printf("HASPAPI_VERSION              : ");

  p1 = p2 = p3 = p4 = 0; /* no input parameters */

  hasp(HASPAPI_VERSION, 0, 0, 0, 0, &p1, &p2, &p3, &p4);

  printf("API version number is %d.%d\n", p4 / 1000, p4 % 1000);

/*****************************************************************************
 * NETHASP_SET_CONFIG_FILENAME
 *   set a config file name
 *   if this service call is omitted, nethasp.ini is used
 */

  strcpy(IniName, "nethasp.ini");
  printf("NETHASP_SET_CONFIG_FILENAME  : \"%s\"  ", IniName);

  p1 = 0;  /* unused */
  p2 = strlen(IniName);

  #if defined(__16BIT__)
    p3 = FP_SEG(IniName);
    p4 = FP_OFF(IniName);
  #elif defined(__64BIT__)
    p4 = (unsigned int)IniName;
    p3 = ((unsigned long)IniName)>>32;
  #else
    p3 = 0;
    p4 = (int)&IniName;
  #endif

  hasp(NETHASP_SET_CONFIG_FILENAME, 0, 1, 0, 0, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************
 * NETHASP_SET_SERVER_BY_NAME
 *   use this Service to specify a particular server LM by name;
 *   if this Service is not used, any server LM with the correct
 *   HASP4 Net key will be used
 *   Note that the name of the LM is NOT necessarily the name of the machine
 */

  if (strlen(ServerName)) {

    p1 = 0;  /* unused */
    p2 = strlen((const char *)ServerName);

    #if defined(__16BIT__)
      p3 = FP_SEG(ServerName);
      p4 = FP_OFF(ServerName);
    #elif defined(__64BIT__)
      p4 = (unsigned int)ServerName;
      p3 = ((unsigned long)ServerName)>>32;
    #else
      p3 = 0;
      p4 = (int)&ServerName;
    #endif

    printf("NETHASP_SET_SERVER_BY_NAME   : servername=\"%s\" : ", ServerName);

    hasp(NETHASP_SET_SERVER_BY_NAME, 0, 1, Pass1, Pass2, &p1, &p2, &p3, &p4);

    if (p3) {
      printf("Failed: status = %d\n", p3);
    } else {
      printf("OK.\n");
    }
  }


/*****************************************************************************
 * NETHASP_LOGIN
 *   logs in to the specified program number
 *
 * NETHASP_LOGIN uses only one login entry per machine,so the new service
 * NETHASP_LOGIN_PROCESS with p1=1 should be used in new applications
 */

  printf("NETHASP_LOGIN                : ");

  p1 = p2 = p3 = p4 = 0; /* no input parameters */

  hasp(NETHASP_LOGIN, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
    exit(1);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************
 * NETHASP_LOGOUT
 *   logs out from specified program number
 */

  printf("NETHASP_LOGOUT               : ");

  hasp(NETHASP_LOGOUT, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3 != HASPERR_SUCCESS)
    printf("Failed: Return status: %d\n", p3);
  else
    printf("OK.\n");

/*****************************************************************************
 * NETHASP_LOGIN_PROCESS
 *   log in to specified program number using the current process' ID,
 *   so multiple instances of the application will produce multiple
 *   login entries.
 *   if p1==0, this call is identical to NETHASP_LOGIN
 *   if p1==1, the process ID is used
 */

  printf("NETHASP_LOGIN_PROCESS (p1=1) : ");

  p1 = 1;   /* use a new login entry for each process */
  p2 = 0;   /* unused */
  p3 = 0;   /* unused */
  p4 = 0;   /* unused */
  hasp(NETHASP_LOGIN_PROCESS, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: status = %d\n", p3);
    exit(1);
  } else  {
    printf("OK.\n");
  }

/*****************************************************************************
 * NETHASP_SETIDLETIME
 *   sets idle time for login entries
 *   (the default value is 2160 minutes = 36 hours)
 */

  {
    int IdleTimeMinutes = 5;  /* idle time to be set, in minutes */

    printf("NETHASP_SETIDLETIME          : %d minutes : ", IdleTimeMinutes);

    p1 = 0; /* unused */
    p2 = 0; /* unused */
    p3 = 0; /* unused */
    p4 = 0; /* unused */
    hasp(NETHASP_SETIDLETIME, IdleTimeMinutes, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

    if (p3) {
      printf("Failed: status = %d\n", p3);
    } else  {
      printf("OK.\n");
    }
  }

/*****************************************************************************
 * NETHASP_QUERY_LICENSE
 *   retrieves information about the current license for
 *   the specified program number
 */

  printf("NETHASP_QUERY_LICENSE        : ");

  {
    int ActiveLogins, MaxLogins, KeyType, Activations;

    hasp(NETHASP_QUERY_LICENSE, SeedCode, ProgNum, Pass1, Pass2,
            &ActiveLogins,   /* number of currently active logins for given ProgNum       */
            &MaxLogins, 		 /* maximum number of concurrent logins for given ProgNum     */
            &KeyType,        /* maximum number of total concurrent logins the key          */
            &Activations);   /* number of remaining acivations (logins) for given ProgNum */

    /* we have to call NETHASP_LASTSTATUS here because license data was returned in p1..p4 */
    hasp(NETHASP_LASTSTATUS, 0, 0, 0, 0, &p1, &p2, &p3, &p4);

    if (p3) {
      printf("Failed: status = %d\n", p3);
    } else {
      printf("Active logins:%5d   Max logins :%5d\n", ActiveLogins, MaxLogins);
      printf("                               "
             "Key Type     :%5d   Activations:%5d\n",  KeyType, Activations);
    }
  }


/*****************************************************************************
 * HASPID
 *   reads the HASP ID Number
 *   The ID Number is a 32 bit integer which is calculated
 *   as follows: ID = p1 + 65536 * p2
 */

  printf("NETHASP_HASPID               : ");

  hasp(NETHASP_HASPID, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed. Error number: %d. ", p3);
  } else {
    long ID;

    ID = p2;
    ID <<= 16;
    ID |= (unsigned) p1;
    printf("%ld (decimal), %#lx (hex)\n", ID, ID);
  }


/*****************************************************************************
 * NETHASP_READBLOCK
 *   reads a memory block
 */

  printf("\nNETHASP_READBLOCK  : ");

  p1 = 0;                        /* start address */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* buffer length in words */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  /* fill buffer with zeroes to ensure that buffer content */
  /* was modified by the READBLOCK service                 */
  memset(&MemoBuffer, 0, sizeof(MemoBuffer));

  hasp(NETHASP_READBLOCK, 0, 1, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(MemoBuffer, MEMO_BUFFER_SIZE, "    ");
    /*dump(MemoBuffer, DATA_LEN, "    ");*/
  }
 exit(0);

/*****************************************************************************
 * MEMOHASP_WRITEBLOCK
 *   writes a block of data to the HASP memory
 */

  printf("\nIncrementing every byte in memory buffer\n");
  /* for ( i = 0; i < MEMO_BUFFER_SIZE; i++ ) MemoBuffer[i]++; */
  for ( i = 0; i < 4; i++ ) MemoBuffer[i]=0;

  printf("NETHASP_WRITEBLOCK : ");
  p1 = 0;                        /* Start address                */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* Buffer length in words       */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  hasp(NETHASP_WRITEBLOCK, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed. Error number: %d.\n", p3);
  } else {
    printf("OK.\n");
  }

/*****************************************************************************
 * NETHASP_READBLOCK
 *   reads a memory block
 */

  printf("\nNETHASP_READBLOCK  : ");

  p1 = 0;                        /* start address */
  p2 = MEMO_BUFFER_SIZE >> 1;    /* buffer length in words */

  #if defined(__16BIT__)
    p3 = FP_SEG(MemoBuffer);
    p4 = FP_OFF(MemoBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)MemoBuffer;
    p3 = ((unsigned long)MemoBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&MemoBuffer;
  #endif

  /* fill buffer with zeroes to ensure that buffer content */
  /* was modified by the READBLOCK service                 */
  memset(&MemoBuffer, 0, sizeof(MemoBuffer));

  hasp(NETHASP_READBLOCK, 0, 1, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(MemoBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * Encoding/decoding Data
 *   (these services are available for HASP4 Net keys only)
 *****************************************************************************/

/*****************************************************************************
 *
 * NETHASP_DECODEDATA
 *   decodes a block of data using the HASP key
 *   block size must be at least 8 bytes
 */

  memcpy(&EncryptionBuffer, &EncryptedData, DATA_LEN);
  printf("\nOriginal encrypted data :\n");
  dump(EncryptionBuffer, DATA_LEN, "    ");

  printf("\nNETHASP_DECODEDATA : ");

  p1 = 0;         /* unused                         */
  p2 = DATA_LEN;  /* number of bytes to decode, >= 8 */

  #if defined(__16BIT__)
    p3 = FP_SEG(EncryptionBuffer);
    p4 = FP_OFF(EncryptionBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)EncryptionBuffer;
    p3 = ((unsigned long)EncryptionBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&EncryptionBuffer;
  #endif

  hasp(NETHASP_DECODEDATA, 0, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(EncryptionBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * NETHASP_ENCODEDATA
 *   encodes a block of data using the HASP key
 *   block size must be at least 8 bytes
 */

  printf("\nNETHASP_ENCODEDATA : ");

  p1 = 0;
  p2 = DATA_LEN;     /* size */

  #if defined(__16BIT__)
    p3 = FP_SEG(EncryptionBuffer);
    p4 = FP_OFF(EncryptionBuffer);
  #elif defined(__64BIT__)
    p4 = (unsigned int)EncryptionBuffer;
    p3 = ((unsigned long)EncryptionBuffer) >> 32;
  #else
    p3 = 0;
    p4 = (int)&EncryptionBuffer;
  #endif

  hasp(NETHASP_ENCODEDATA, 0, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3) {
    printf("Failed: Return status: %d\n", p3);
  } else {
    printf("OK.\n");
    dump(EncryptionBuffer, DATA_LEN, "    ");
  }

/*****************************************************************************
 * NETHASP_LOGOUT
 *   logs out from specified program number
 */

  printf("NETHASP_LOGOUT               : ");

  hasp(NETHASP_LOGOUT, SeedCode, ProgNum, Pass1, Pass2, &p1, &p2, &p3, &p4);

  if (p3 != HASPERR_SUCCESS)
    printf("Failed: Return status: %d\n", p3);
  else
    printf("OK.\n");

/*****************************************************************************/

  return 0;

}

/* eof */

