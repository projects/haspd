#!/bin/sh -x
# 2006, 2007, 2021 (c) Etersoft http://etersoft.ru
# Author: Vitaly Lipatov <lav@etersoft.ru>
# GNU Public License

fatal()
{
	echo $@
	exit 1
}


DISTR_VENDOR=/usr/bin/distr_vendor
test -x $DISTR_VENDOR || DISTR_VENDOR=/usr/bin/distro_info
test -x $DISTR_VENDOR || fatal "Can't find distr_vendor"


LIB=$1
ARCHSUFFIX="_$($DISTR_VENDOR -a)"
[ "$ARCHSUFFIX" = "_x86" ] && ARCHSUFFIX=""
[ "$ARCHSUFFIX" = "_aarch64" ] && ARCHSUFFIX="_arm64"

mkdir -p $SBIN_DIR

install -m755 -D haspd.init $INIT_DIR/haspd
install -m755 haspd.outformat $INIT_DIR/

subst "s|export MODULEVERSION=.*|export MODULEVERSION=$MODULEVERSION|g" $INIT_DIR/haspd

# Aladdin
#install -m755 winehasp/winehasp $SBIN_DIR/
if [ -f aksusbd/aksusbd$ARCHSUFFIX ] ; then
    install -m755 aksusbd/aksusbd$ARCHSUFFIX $SBIN_DIR/aksusbd
fi

install -m755 hasplmd/hasplmd$ARCHSUFFIX $SBIN_DIR/hasplmd
install -m755 -d $SBIN_DIR/../bin

mkdir -p $SBIN_DIR/../../etc/hasplm/
cp -a hasplmd/templates $SBIN_DIR/../../etc/hasplm/

mkdir -p $SBIN_DIR/../../etc/sysconfig/
install -m644 haspd.conf $SBIN_DIR/../../etc/sysconfig/haspd

#install -m755 etersoft/setnethasp $SBIN_DIR/../bin

cd usbkeytest
make || echo "false" >usbkeytest
install -m755 usbkeytest $SBIN_DIR/usbkeytest
cd -

exit 0
