#include "config.h"

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <getopt.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#ifdef HAVE_LIBUSB_H
#include <libusb.h>
#else
#include <usb.h>
#endif

struct key_ids {
    u_int16_t idVendor;
    u_int16_t idProduct;
    char name[256];
    int presence; /* Не 0, если ключ есть */
};

struct dev_desc {
    int bDeviceClass;
    int bDeviceSubClass;
    int bDeviceProtocol;
};

struct dev_interf {
    int bNumEndpoints;
    int bInterfaceClass;
    int bInterfaceSubClass;
    int bInterfaceProtocol;
};

struct dev_conf {
    int bNumInterfaces;
    struct dev_interf *interf;
};

struct dev_type {
    struct dev_desc desc;
    struct dev_conf conf;
};

/* Идентификаторы и имена ключей */
/* http://www.linux-usb.org/usb.ids */
struct key_ids keys[] = {
    {0x0529, 0x0001, "aladdin", 0},
    /*
    {0x0529, 0x030b, "aladdin", 0},
    {0x0529, 0x0313, "aladdin", 0},
    {0x0529, 0x031b, "aladdin", 0},
    {0x0529, 0x0323, "aladdin", 0},
    {0x0529, 0x0412, "aladdin", 0},
    {0x0529, 0x041a, "aladdin", 0},
    {0x0529, 0x0422, "aladdin", 0},
    {0x0529, 0x042a, "aladdin", 0},
    {0x0529, 0x050c, "aladdin", 0},
    {0x0529, 0x0514, "aladdin", 0},
    {0x073d, 0x0005, "eutron", 0},
    {0x073d, 0x0007, "eutron", 0},
    */
    {0x073d, 0x0025, "eutron", 0},
    {0x04b9, 0x0300, "sentinel", 0}
};

int list_key = 0;
int detect_key = 0;
int add_n = 0; /* Если не 0, то при завершении программы будет выведен '\n' */
int exit_code = 0;

void print_major_and_minor(const char *bus, const char *dev)
{
    static char path[PATH_MAX];
    struct stat st;

    sprintf(path, "/dev/bus/usb/%s/%s", bus, dev);
    if (!stat(path, &st))
        printf("Major: %d, Minor: %d ", major(st.st_rdev), minor(st.st_rdev));
    else {
        printf("Can't locate %s", path);
        exit_code = 1;
    }
}

#ifdef HAVE_LIBUSB_H

void print_id(libusb_device *dev)
{
    struct libusb_device_descriptor desc;
    char bus[4], device[4];

    if (libusb_get_device_descriptor(dev, &desc)) {
        printf("ID: ????:????, ");
        exit_code = 1;
    }
    else {
        printf("ID: %04X:%04X, ",
            desc.idVendor,
            desc.idProduct);
    }

    sprintf(bus, "%03u", libusb_get_bus_number(dev));
    sprintf(device,"%03u", libusb_get_device_address(dev));
    printf("Bus: %s, ", bus);
    printf("Device: %s, ", device);
    print_major_and_minor(bus, device);
    puts("");
}

void print_name(libusb_device *dev)
{
    struct libusb_device_descriptor desc;
    unsigned int i;

    if (libusb_get_device_descriptor(dev, &desc)) {
        exit_code = 1;
        return;
    }
    for (i = 0; i < sizeof(keys)/sizeof(struct key_ids); ++i)
        if (desc.idVendor == keys[i].idVendor &&
            desc.idProduct == keys[i].idProduct) {
            if (!keys[i].presence)
                printf("%s ", keys[i].name);
            keys[i].presence = 1;
            add_n = 1;
        }
}

/* Сравнивается только одна конфигурация устройства */
/* Поля, значения которых меньше 0, не сравинваются */
int is_dev_type(libusb_device *dev, struct dev_type *type)
{
    struct libusb_device_descriptor desc;
    struct libusb_config_descriptor *conf;
    unsigned int i;

    if (libusb_get_device_descriptor(dev, &desc)) {
        exit_code = 1;
        return 0;
    }
    if (libusb_get_config_descriptor(dev, 0, &conf)) {
        exit_code = 1;
        return 0;
    }

    if (type->desc.bDeviceClass > 0)
        if (desc.bDeviceClass != type->desc.bDeviceClass)
            goto ret0;
    if (type->desc.bDeviceSubClass > 0)
        if (desc.bDeviceSubClass != type->desc.bDeviceSubClass)
            goto ret0;
    if (type->desc.bDeviceProtocol > 0)
        if (desc.bDeviceProtocol != type->desc.bDeviceProtocol)
            goto ret0;
    if (type->conf.bNumInterfaces > 0)
        if (conf->bNumInterfaces != type->conf.bNumInterfaces)
            goto ret0;
    for (i = 0; i < conf->bNumInterfaces; ++i) {
        if (type->conf.interf[i].bNumEndpoints > 0)
            if (conf->interface[i].altsetting[0].bNumEndpoints !=
                type->conf.interf[i].bNumEndpoints)
                goto ret0;
        if (type->conf.interf[i].bInterfaceClass > 0)
            if (conf->interface[i].altsetting[0].bInterfaceClass !=
                type->conf.interf[i].bInterfaceClass)
                goto ret0;
        if (type->conf.interf[i].bInterfaceSubClass > 0)
            if (conf->interface[i].altsetting[0].bInterfaceSubClass !=
                type->conf.interf[i].bInterfaceSubClass)
                goto ret0;
        if (type->conf.interf[i].bInterfaceProtocol > 0)
            if (conf->interface[i].altsetting[0].bInterfaceProtocol !=
                type->conf.interf[i].bInterfaceProtocol)
                goto ret0;
    }

    libusb_free_config_descriptor(conf);
    return 1;
ret0:
    libusb_free_config_descriptor(conf);
    return 0;
}

/* Возвращает 1, если dev - коммуникационное устройство */
int is_cdc(libusb_device *dev)
{
    struct dev_interf direct_line[] = {{-1, 2, 1, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf abstract[] = {{-1, 2, 2, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf phone[] = {{-1, 2, 3, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf multi[] = {{-1, 2, 4, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf capi[] = {{-1, 2, 5, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf eth[] = {{-1, 2, 6, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf atm[] = {{-1, 2, 7, -1}, {-1, 0xA, 0, -1}};
    struct dev_type types[] = {
        {{2, 0, 0}, {2, direct_line}},
        {{2, 0, 0}, {2, abstract}},
        {{2, 0, 0}, {2, phone}},
        {{2, 0, 0}, {2, multi}},
        {{2, 0, 0}, {2, capi}},
        {{2, 0, 0}, {2, eth}},
        {{2, 0, 0}, {2, atm}}
    };
    unsigned int i;

    for (i = 0; i < sizeof(types)/sizeof(struct dev_type); ++i)
        if (is_dev_type(dev, &(types[i])))
            return 1;

    return 0;
}

/* Возвращает 1, если dev - хаб */
int is_hub(libusb_device *dev)
{
    struct dev_interf hub_interf = {1, 9, 0, 0};
    struct dev_type hub_dev = {{9, 0, -1}, {-1, &hub_interf}};

    if (is_dev_type(dev, &hub_dev))
        return 1;

    return 0;
}

/* Возвращает 1, если dev - HID-устройство */
int is_hid(libusb_device *dev)
{
    struct libusb_config_descriptor *conf;
    unsigned int i;

    if (libusb_get_config_descriptor(dev, 0, &conf)) {
        exit_code = 1;
        return 0;
    }
    for (i = 0; i < conf->bNumInterfaces; ++i)
        if (conf->interface[i].altsetting[0].bInterfaceClass != 3)
            return 0;
    libusb_free_config_descriptor(conf);

    return 1;
}

int test_type(libusb_device *dev)
{
    if (is_cdc(dev))
        return 0;
    if (is_hid(dev))
        return 0;
    if (is_hub(dev))
        return 0;

    return 1;
}

void print_dev(libusb_device *dev)
{
    if (test_type(dev)) {
        if (list_key)
            print_id(dev);
        if (detect_key)
            print_name(dev);
    }
}

#else

void print_id(struct usb_device *dev)
{
    printf("Bus %s ",
        dev->bus->dirname);
    printf("Device %s: ",
        dev->filename);
    print_major_and_minor(dev->bus->dirname, dev->filename);
    printf("ID %04X:%04X\n",
        dev->descriptor.idVendor,
        dev->descriptor.idProduct);
}

void print_name(struct usb_device *dev)
{
    unsigned int i;

    for (i = 0; i < sizeof(keys)/sizeof(struct key_ids); ++i)
        if (dev->descriptor.idVendor == keys[i].idVendor &&
            dev->descriptor.idProduct == keys[i].idProduct) {
            if (!keys[i].presence)
                printf("%s ", keys[i].name);
            keys[i].presence = 1;
            add_n = 1;
        }
}

/* Сравнивается только одна конфигурация устройства */
/* Поля, значения которых меньше 0, не сравинваются */
int is_dev_type(struct usb_device *dev, struct dev_type *type)
{
    unsigned int i;

    if (type->desc.bDeviceClass > 0)
        if (dev->descriptor.bDeviceClass != type->desc.bDeviceClass)
            return 0;
    if (type->desc.bDeviceSubClass > 0)
        if (dev->descriptor.bDeviceSubClass != type->desc.bDeviceSubClass)
            return 0;
    if (type->desc.bDeviceProtocol > 0)
        if (dev->descriptor.bDeviceProtocol != type->desc.bDeviceProtocol)
            return 0;
    if (type->conf.bNumInterfaces > 0)
        if (dev->config[0].bNumInterfaces != type->conf.bNumInterfaces)
            return 0;
    for (i = 0; i < dev->config[0].bNumInterfaces; ++i) {
        if (type->conf.interf[i].bNumEndpoints > 0)
            if (dev->config[0].interface[i].altsetting[0].bNumEndpoints !=
                type->conf.interf[i].bNumEndpoints)
                return 0;
        if (type->conf.interf[i].bInterfaceClass > 0)
            if (dev->config[0].interface[i].altsetting[0].bInterfaceClass !=
                type->conf.interf[i].bInterfaceClass)
                return 0;
        if (type->conf.interf[i].bInterfaceSubClass > 0)
            if (dev->config[0].interface[i].altsetting[0].bInterfaceSubClass !=
                type->conf.interf[i].bInterfaceSubClass)
                return 0;
        if (type->conf.interf[i].bInterfaceProtocol > 0)
            if (dev->config[0].interface[i].altsetting[0].bInterfaceProtocol !=
                type->conf.interf[i].bInterfaceProtocol)
                return 0;
    }

    return 1;
}

/* Возвращает 1, если dev - коммуникационное устройство */
int is_cdc(struct usb_device *dev)
{
    struct dev_interf direct_line[] = {{-1, 2, 1, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf abstract[] = {{-1, 2, 2, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf phone[] = {{-1, 2, 3, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf multi[] = {{-1, 2, 4, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf capi[] = {{-1, 2, 5, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf eth[] = {{-1, 2, 6, -1}, {-1, 0xA, 0, -1}};
    struct dev_interf atm[] = {{-1, 2, 7, -1}, {-1, 0xA, 0, -1}};
    struct dev_type types[] = {
        {{2, 0, 0}, {2, direct_line}},
        {{2, 0, 0}, {2, abstract}},
        {{2, 0, 0}, {2, phone}},
        {{2, 0, 0}, {2, multi}},
        {{2, 0, 0}, {2, capi}},
        {{2, 0, 0}, {2, eth}},
        {{2, 0, 0}, {2, atm}}
    };
    unsigned int i;

    for (i = 0; i < sizeof(types)/sizeof(struct dev_type); ++i)
        if (is_dev_type(dev, &(types[i])))
            return 1;

    return 0;
}

/* Возвращает 1, если dev - хаб */
/* Про идентификацию хаба можно прочитать здесь:
   http://masters.donntu.edu.ua/2004/fvti/pavliy/library/article1.htm */
int is_hub(struct usb_device *dev)
{
    struct dev_interf hub_interf = {1, 9, 0, 0};
    struct dev_type hub_dev = {{9, 0, -1}, {-1, &hub_interf}};

    if (is_dev_type(dev, &hub_dev))
        return 1;

    return 0;
}

/* Возвращает 1, если dev - HID-устройство */
int is_hid(struct usb_device *dev)
{
    unsigned int i;

    for (i = 0; i < dev->config[0].bNumInterfaces; ++i)
        if (dev->config[0].interface[i].altsetting[0].bInterfaceClass != 3)
            return 0;

    return 1;
}

int test_type(struct usb_device *dev)
{
    if (is_cdc(dev))
        return 0;
    if (is_hid(dev))
        return 0;
    if (is_hub(dev))
        return 0;

    return 1;
}

void print_dev(struct usb_device *dev)
{
    if (test_type(dev)) {
        if (list_key)
            print_id(dev);
        if (detect_key)
            print_name(dev);
    }
}

#endif

void print_usage(char *progname)
{
    fprintf(stderr, "Usage: %s --list | --detect\n", progname);
}

int main(int argc, char *argv[])
{
    struct option longopts[] = {
        {"list", 0, &list_key, 1},
        {"detect", 0, &detect_key, 1},
        {0, 0, 0, 0}
    };
    int longindex = 0,
        opt;
#ifdef HAVE_LIBUSB_H
    unsigned int i;
    ssize_t cnt;
    libusb_device **devs;
#else
    struct usb_bus *bus;
    struct usb_device *dev;
#endif

    do {
        opt = getopt_long(argc, argv, "", longopts, &longindex);
        if (opt == '?')
            return 1;
    } while (opt != -1);

    if ((list_key && detect_key) || (!list_key && !detect_key)) {
        print_usage(argv[0]);
        return 1;
    }

#ifdef HAVE_LIBUSB_H
    if (libusb_init(NULL))
        return 1;

    cnt = libusb_get_device_list(NULL, &devs);
    if (cnt < 0) {
        libusb_exit(NULL);
        return 1;
    }
    for (i = 0; i < cnt; ++i)
        print_dev(devs[i]);
    libusb_free_device_list(devs, 1);

    libusb_exit(NULL);
#else
    usb_init();

    usb_find_busses();
    usb_find_devices();

    for (bus = usb_busses; bus; bus = bus->next)
        for (dev = bus->devices; dev; dev = dev->next)
            print_dev(dev);
#endif
    if (add_n)
        printf("\n");

    return exit_code;
}
